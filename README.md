# Greyhound

K8S manifest files for home server uses.

```bash
kubectl -n kube-system create sa tiller && kubectl create clusterrolebinding tiller --clusterrole cluster-admin --serviceaccount=kube-system:tiller
helm init --service-account tiller
sudo sshfs -o allow_other jteague@redenbacher.lan:/home/jteague/plex /media/plex
kubectl apply -f https://raw.githubusercontent.com/jetstack/cert-manager/release-0.6/deploy/manifests/00-crds.yaml
kubectl create ns cert-manager
kubectl label namespace cert-manager certmanager.k8s.io/disable-validation="true"
helm install --name cert-manager --namespace cert-manager stable/cert-manager
helm install --name nfs-server --namespace kube-system stable/nfs-server-provisioner --set persistence.enabled=true --set persistence.size=100Gi --set persistence.storageClass=hostmount --set storageClass.default=true
```